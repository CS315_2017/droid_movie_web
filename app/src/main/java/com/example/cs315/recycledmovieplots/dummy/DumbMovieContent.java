package com.example.cs315.recycledmovieplots.dummy;

import com.example.cs315.recycledmovieplots.models.MovieModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by richtanner on 11/9/17.
 */

public class DumbMovieContent {

    /**
     * A map of the Movie items, by ID (title).
     */
    public static final Map<String, MovieModel> ITEM_MAP = new HashMap<String, MovieModel>();

    /**
     * A List of the Movie items.
     */
    public static final List<MovieModel> MOVIES = new ArrayList<MovieModel>();


    /**
     * Create all those movie Strings we will be needing for teh models
     */

    // CS315: DO THIS
    // TODO: Create five new movie objects here (you can delete the two existing). Complete with images and URLs.

    private static final String movie1Title = "Justice League";
    private static final String movie1Description = "Fueled by his restored faith in humanity and inspired by Superman's selfless act, Bruce Wayne enlists the help of his newfound ally, Diana Prince, to face an even greater enemy. \n \nThis had BETTER not be terrible!";
    private static final String movie1Year = "2017";
    private static final String movie1Image = "jleague";
    private static final String movie1Weblink = "http://www.imdb.com/title/tt0974015/?ref_=nv_sr_1";

    private static final String thorTitle = "Thor: Ragnarok";
    private static final String thorDescription = "Imprisoned, the almighty Thor finds himself in a lethal gladiatorial contest against the Hulk, his former ally. Thor must fight for survival and race against time to prevent the all-powerful Hela from destroying his home and the Asgardian civilization. \n \nSO much flippin' fun!!";
    private static final String thorYear = "2017";
    private static final String thorImage = "thor";
    private static final String thorWeblink = "http://www.imdb.com/title/tt3501632/?ref_=nv_sr_1";


    /**
     * Create and return an array of Movie items.  Duh!
     */
    public List<MovieModel> createMovieMagic () {

        // make those movie objects
        MovieModel thor = new MovieModel(thorTitle, thorDescription, thorYear, thorImage, thorWeblink);
        MovieModel justiceLeague = new MovieModel(movie1Title, movie1Description, movie1Year, movie1Image, movie1Weblink);

        // add EACH movie object to our lists and maps
        addMovieToList(thor);
        addMovieToList(justiceLeague);

        // no more movies to add?  Okay... return our list
        return MOVIES;
    }

    // Internal helper so we don't forget any steps in the complex two-step system.  Seriously.  It happens.
    private void addMovieToList (MovieModel datMovie) {
        MOVIES.add(datMovie);
        ITEM_MAP.put(datMovie.getMovieTitle(), datMovie);
    }

    /* initializer */
    public DumbMovieContent() {
        createMovieMagic();
    }

}
